package com.example;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import static org.junit.Assert.*;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class NavigationTest {
	
	WebDriver driver;
	
	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "./resources/webdrivers/chromedriver");
		driver = new ChromeDriver();
		driver.get("http://magento-demo.lexiconn.com/");
//		driver.get("http://the-internet.herokuapp.com");
	}
		
	@Test
	public void navigateToAUrl() {
//		driver.get("http://www.google.com");
		driver.get("http://magento-demo.lexiconn.com/");
//		driver.get("http://demo-store.seleniumacademy.com/");
		Assert.assertEquals(driver.getTitle(), "Madison Island");
	}

	@Test
	// this action is applicable for textbox or textarea HTML elements
	public void searchProduct() {
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.sendKeys("Phones");
//		searchBox.submit();
		WebElement searchButton = driver.findElement(By.className("search-button"));
		searchButton.click();
		assertThat(driver.getTitle()).isEqualTo("Search results for: 'Phones'");
	}

	@Test
	public void elementSendKeysCompositionExample() {
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.sendKeys(Keys.chord(Keys.SHIFT,"phones"));
		searchBox.submit();
		assertThat(driver.getTitle()).isEqualTo("Search results for: 'PHONES'");
	}

	@Test
	public void byClassNameLocatorExample() {
		WebElement searchBox = driver.findElement(By.id("search"));
		searchBox.sendKeys("Electronics");
		WebElement searchButton = driver.findElement(By.className("search-button"));
		searchButton.click();
		assertThat(driver.getTitle()).isEqualTo("Search results for: 'Electronics'");
	}
	
	@Test
	public void byLinkTextLocatorExample() {
		WebElement myAccountLink = driver.findElement(By.linkText("MY ACCOUNT"));
		myAccountLink.click();
		assertThat(driver.getTitle()).isEqualTo("Customer Login");
	}
	
	@Test
	public void byPartialLinkTextLocatorExample() {
		WebElement orderAndReturns = driver.findElement(By.partialLinkText("PRIVACY"));
		orderAndReturns.click();
		assertThat(driver.getTitle()).isEqualTo("Privacy Policy");
	}
	
	@Test
	public void byTagNameLocatorExample() {
		List<WebElement> links = driver.findElements(By.tagName("a"));
		System.out.println("Found links:" + links.size());
		links.stream()
					.filter(elem -> elem.getText().length() > 0)
					.forEach(elem -> System.out.println(elem.getText()));
	}
	
	@Test
	public void byXPathLocatorElement() {
		WebElement searchBox = driver.findElement(By.xpath("//*[@id='search']"));
		searchBox.sendKeys("Bags");
		searchBox.submit();
		assertThat(driver.getTitle()).isEqualTo("Search results for: 'Bags'");
	}

	// page 41
	@Test
	public void byCssSelectorLocatorExample() {
		WebElement searchBox = driver.findElement(By.cssSelector("#search"));
		searchBox.sendKeys("Bags");
		searchBox.submit();
		assertThat(driver.getTitle()).isEqualTo("Search results for: 'Bags'");
	}

	// page 42
	@Test
	public void byCssSelectorLocatorComplexExample() {
		WebElement aboutUs = driver.findElement(By.cssSelector("a[href*='/about-magento-demo-store/']"));
		aboutUs.click();
		assertThat(driver.getTitle()).isEqualTo("About Us");
	}

	// page 43
	@Test
	public void elementGetAttributesExample() {
		WebElement searchBox = driver.findElement(By.name("q"));
		System.out.println("Name of the box is: " + searchBox.getAttribute("name"));
		System.out.println("Id of the box is: " + searchBox.getAttribute("id"));
		System.out.println("Class of the box is: " + searchBox.getAttribute("class"));
		System.out.println("Placeholder of the box is: " + searchBox.getAttribute("placeholder"));
	}
	/*
	 * 	Name of the box is: q
		Id of the box is: search
		Class of the box is: input-text required-entry
		Placeholder of the box is: Search entire store here...
	 */
	
	@Test	// page 44
	public void elementGetTextExample() {
		WebElement siteNotice = driver.findElement(By.className("global-site-notice"));
		System.out.println("Complete text is: " + siteNotice.getText());
	}

	@Test // page 45
	// the API syntax for the getCssValue() method is as follow:
	// java.lang.String getCssValue(java.lang.String propertyName);
	public void elementGetCssValueExample() {
		WebElement searchBox = driver.findElement(By.name("q"));
		System.out.println("Font of the box is: " + searchBox.getCssValue("font-family"));
	}
	// Font of the box is: Raleway, "Helvetica Neue", Verdana, Arial, sans-serif

	@Test // page 45-46
	// the API syntax of the getLocation() method is: Point getLocation()
	public void getLocation() {
		WebElement searchBox = driver.findElement(By.name("q"));
		System.out.println("Location of the box is: " + searchBox.getLocation());
		// (x,y) location of the search box is (885, 137)
	}

	@Test // page 46
	// this method return the width and height of the rendered WebElement
	// the API syntax -> Dimension getSize()
	public void getSize() {
		WebElement searchBox = driver.findElement(By.name("q"));
		System.out.println("Size of the box is: " + searchBox.getSize());
	}

	@Test //page 46-47
	// the API syntax for the getTagName() method is: java.lang.String getTagName()
	public void elementGetTagName() {
		WebElement searchButton = driver.findElement(By.className("search-button"));
		System.out.println("Html tag of the button is: " + searchButton.getTagName());
	}


	
	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
